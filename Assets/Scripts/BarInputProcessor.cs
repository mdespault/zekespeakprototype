﻿using UnityEngine;
using UnityEngine.Assertions;

namespace Assets.Scripts
{
    public class BarInputProcessor : MonoBehaviour, Constants.IBarInputHandler
    {
        private GameSettings _gameSettings;
        private Constants.IInputHandler _inputManager;
        private GameObject _leftBarAttachPoint;
        private GameObject _rightBarAttachPoint;

        public void RegisterLeftBarAttachPoint(GameObject leftBarAttachPoint)
        {
            _leftBarAttachPoint = leftBarAttachPoint;
        }

        public void RegisterRightBarAttachPoint(GameObject rightBarAttachPoint)
        {
            _rightBarAttachPoint = rightBarAttachPoint;
        }

        private void GatherInterfaceProviders()
        {
            _inputManager = InterfaceHelper.FindObject<Constants.IInputHandler>();
            Assert.IsTrue(_inputManager != null);
        }

        private void GatherComponentReferences()
        {
            _gameSettings = FindObjectOfType(typeof (GameSettings)) as GameSettings;
            Assert.IsTrue(_gameSettings != null);
        }

        public void Awake()
        {
            GatherComponentReferences();
            GatherInterfaceProviders();
        }

        private void RegisterInputHandling()
        {
            _inputManager.RegisterBarInputProcessor(Update);
        }

        // Use this for initialization
        private void Start()
        {
        }

        // Update is called once per frame
        private void Update()
        {
            if (Input.GetKey(_gameSettings.LeftBarUpKey))
            {
                if (_leftBarAttachPoint != null)
                {
                    _leftBarAttachPoint.transform.position += _leftBarAttachPoint.transform.up*
                                                              _gameSettings.MovementSpeed;
                }
            }
            if (Input.GetKey(_gameSettings.LeftBarDownKey))
            {
                if (_leftBarAttachPoint != null)
                {
                    _leftBarAttachPoint.transform.position += -_leftBarAttachPoint.transform.up*
                                                              _gameSettings.MovementSpeed;
                }
            }
            if (Input.GetKey(_gameSettings.RightBarUpKey))
            {
                if (_rightBarAttachPoint != null)
                {
                    _rightBarAttachPoint.transform.position += _rightBarAttachPoint.transform.up*
                                                               _gameSettings.MovementSpeed;
                }
            }
            if (Input.GetKey(_gameSettings.RightBarDownKey))
            {
                if (_rightBarAttachPoint != null)
                {
                    _rightBarAttachPoint.transform.position += -_rightBarAttachPoint.transform.up*
                                                               _gameSettings.MovementSpeed;
                }
            }
        }
    }
}