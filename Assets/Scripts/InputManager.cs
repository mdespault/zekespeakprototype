﻿using UnityEngine;

namespace Assets.Scripts
{
    public class InputManager : MonoBehaviour, Constants.IInputHandler
    {
        private Constants.GenericCallback _barInputProcessor;
        private bool _playerInputEnabled;

        public void EnablePlayerInput(bool enable)
        {
            _playerInputEnabled = enable;
        }

        public void RegisterBarInputProcessor(Constants.GenericCallback barInputProcessor)
        {
            _barInputProcessor += barInputProcessor;
        }

        private void Awake()
        {
        }

        public void Update()
        {
            if (_playerInputEnabled)
            {
                if (_barInputProcessor != null)
                {
                    _barInputProcessor();
                }
            }
        }
    }
}