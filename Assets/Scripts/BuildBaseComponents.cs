﻿using UnityEngine;
using UnityEngine.Assertions;

namespace Assets.Scripts
{
    public class BuildBaseComponents : MonoBehaviour
    {
        private Constants.IBarInputHandler _barInputProcessor;

        private GameObject _cylinder;
        private GameSettings _gameSettings;
        private SliderComponent _leftHorizontalSlider;
        private GameObject _piston;
        private SliderComponent _rightHorizontalSlider;
        public GameObject AttachmentPointRef;
        public GameObject CylinderComponentRef;

        public GameObject HorizontalSliderRef;
        public GameObject PistonRef;
        public GameObject StopperRef;

        private void GatherComponentReferences()
        {
            _gameSettings = FindObjectOfType(typeof (GameSettings)) as GameSettings;
            Assert.IsTrue(_gameSettings != null);
        }

        private void GatherInterfaceProviders()
        {
            _barInputProcessor = InterfaceHelper.FindObject<Constants.IBarInputHandler>();
            Assert.IsTrue(_barInputProcessor != null);
        }

        private void Awake()
        {
            Assert.IsNotNull(StopperRef, "Missing reference to Stopper prefab");
            Assert.IsNotNull(AttachmentPointRef, "Missing reference to AttachmentPoint prefab");
            Assert.IsNotNull(HorizontalSliderRef, "Missing reference to HorizontalSlider prefab");
            Assert.IsNotNull(PistonRef, "Missing reference to Piston prefab");
            Assert.IsNotNull(CylinderComponentRef, "Missing reference to CylinderComponentRef prefab");

            GatherInterfaceProviders();
            GatherComponentReferences();
        }

        // Use this for initialization
        private void Start()
        {
            BuildComponents();
            RegisterMovableComponents();
        }

        // Update is called once per frame
        private void Update()
        {
            UpdatePistonLocation();
        }

        private void UpdatePistonLocation()
        {
            Vector3 leftAttachmentPointPosition = _leftHorizontalSlider.AttachmentPoint.transform.position;
            Vector3 rightAttachmentPointPosition = _rightHorizontalSlider.AttachmentPoint.transform.position;

            Vector3 vectorBetweenPoints = rightAttachmentPointPosition - leftAttachmentPointPosition;
            float distanceBetweenAttachmentPoints =
                Vector3.Magnitude(vectorBetweenPoints);

            Vector3 pistonScale = _piston.transform.localScale;
            _piston.transform.localScale = new Vector3(pistonScale.x, distanceBetweenAttachmentPoints, pistonScale.z);

            Vector3 yMidPoint = vectorBetweenPoints/2 + leftAttachmentPointPosition;
            _piston.transform.position = new Vector3(_piston.transform.position.x, yMidPoint.y,
                _piston.transform.position.z);
            _piston.transform.localRotation = Quaternion.FromToRotation(Vector3.down, vectorBetweenPoints);

            _cylinder.transform.position = _piston.transform.position;
            _cylinder.transform.rotation = _piston.transform.rotation;
        }

        private GameObject BuildHorizontalSlider()
        {
            GameObject horizontalSlider = Instantiate(HorizontalSliderRef);
            Vector3 originalScale = horizontalSlider.transform.localScale;
            horizontalSlider.transform.localScale = new Vector3(originalScale.x, _gameSettings.HeightScale,
                originalScale.z);

            return horizontalSlider;
        }

        private SliderComponent BuildSliderComponent()
        {
            GameObject slider = BuildHorizontalSlider();
            float heightScale = slider.transform.localScale.y;

            GameObject topStopper = Instantiate(StopperRef, slider.transform, true);
            topStopper.transform.Translate(0, heightScale/2, 0);

            GameObject bottomStopper = Instantiate(StopperRef, slider.transform, true);
            bottomStopper.transform.Translate(0, -heightScale/2, 0);

            GameObject attachmentPoint = Instantiate(AttachmentPointRef, slider.transform, true);
            return new SliderComponent(slider, topStopper, bottomStopper, attachmentPoint);
        }

        private void BuildComponents()
        {
            _leftHorizontalSlider = BuildSliderComponent();
            _leftHorizontalSlider.Slider.transform.Translate(-_gameSettings.WidthScale/2, 0, 0);

            _rightHorizontalSlider = BuildSliderComponent();
            _rightHorizontalSlider.Slider.transform.Translate(_gameSettings.WidthScale/2, 0, 0);

            _piston = Instantiate(PistonRef);
            _cylinder = Instantiate(CylinderComponentRef, _piston.transform, true);
            UpdatePistonLocation();
            _cylinder.transform.parent = null;
        }

        private void RegisterMovableComponents()
        {
            _barInputProcessor.RegisterLeftBarAttachPoint(_leftHorizontalSlider.AttachmentPoint);
            _barInputProcessor.RegisterRightBarAttachPoint(_rightHorizontalSlider.AttachmentPoint);
        }

        public struct SliderComponent
        {
            public GameObject Slider;
            public GameObject AttachmentPoint;
            public GameObject TopStopper;
            public GameObject BottomStopper;

            public SliderComponent(GameObject slider, GameObject topStopper, GameObject bottomStopper,
                GameObject attachmentPoint)
            {
                Slider = slider;
                TopStopper = topStopper;
                BottomStopper = bottomStopper;
                AttachmentPoint = attachmentPoint;
            }
        }
    }
}