﻿using UnityEngine;

namespace Assets.Scripts
{
    public class Constants : MonoBehaviour {

        public delegate void GenericCallback();

        public interface IInputHandler
        {
            void RegisterBarInputProcessor(GenericCallback inputProcessor);
            void EnablePlayerInput(bool enable);
        }

        public interface IBarInputHandler
        {
            void RegisterLeftBarAttachPoint(GameObject leftBarAttachPoint);
            void RegisterRightBarAttachPoint(GameObject rightBarAttachPoint);
        }

        public enum BarControlType
        {
            LeftBarUp,
            LeftBarDown,
            RightBarUp,
            RightBarDown
        }

    }
}