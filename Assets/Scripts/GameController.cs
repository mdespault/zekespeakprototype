﻿using Assets.Scripts;
using MonsterLove.StateMachine;
using UnityEngine;
using UnityEngine.Assertions;

public class GameController : StateBehaviour
{
    private GameSettings _gameSettings;
    private Constants.IInputHandler _inputManager;
    //    private Constants.GenericCallback _inputProcessor;

    public void ProcessInput()
    {
        //       _inputProcessor();
    }

    private void RegisterInputProcessing()
    {
        //            _inputManager.RegisterTouchInputProcessor(HandleTouchInput);
    }

    private void GatherInterfaceProviders()
    {
        _inputManager = InterfaceHelper.FindObject<Constants.IInputHandler>();
        Assert.IsTrue(_inputManager != null);
    }

    private void GatherComponentReferences()
    {
        _gameSettings = FindObjectOfType(typeof (GameSettings)) as GameSettings;
    }

    public void Awake()
    {
        GatherInterfaceProviders();
        GatherComponentReferences();
        RegisterInputProcessing();

        Initialize<States>();
    }

    private void SetApplicationSettings()
    {
        Application.targetFrameRate = 30;
        QualitySettings.vSyncCount = 0;
        Assert.raiseExceptions = true;
    }

    public void Start()
    {
        SetApplicationSettings();
        ChangeState(States.Init);
    }

    #region Init

    private void Init_Enter()
    {
        // Delay to allow things to register
        ChangeState(States.GameStart);
    }

    #endregion Init

    private enum States
    {
        Init,
        GameStart
    }

    #region GameStart

    private void StartGame()
    {
        //        _inputProcessor = () => { };
        _inputManager.EnablePlayerInput(true);
    }

    private void GameStart_Enter()
    {
    }

    private void GameStart_Exit()
    {
    }

    #endregion GameStart
}