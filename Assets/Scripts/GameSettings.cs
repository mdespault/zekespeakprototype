﻿using UnityEngine;

namespace Assets.Scripts
{
    public class GameSettings : MonoBehaviour
    {
        [Header("Input Controls")]
        public KeyCode LeftBarUpKey = KeyCode.Q;

        public KeyCode LeftBarDownKey = KeyCode.A;
        public KeyCode RightBarUpKey = KeyCode.F;
        public KeyCode RightBarDownKey = KeyCode.S;

        [Header("General Settings")]
        public float MovementSpeed = 0.1f;

        [Header("Bar Settings")]
        public float HeightScale = 1.0f;

        public float WidthScale = 1.0f;
    }
}